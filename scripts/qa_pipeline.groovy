pipeline {
    agent any
    options {
        lock label: 'qa-eastasia', quantity: 1
    }
    stages {
        stage('will_already_be_locked') {
            steps {
                sleep 90
            }
        }
        stage('will_also_be_locked') {
            steps {
                echo "I am still locked!"
            }
        }
    }
}