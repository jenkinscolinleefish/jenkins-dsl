import groovy.json.JsonSlurper
import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import groovy.transform.Canonical
import groovy.transform.Field
import hudson.*

def MAX_BUILDS=40

SCRIPT_DIR = "${new File(__FILE__).parent}"

File f = new File("${SCRIPT_DIR}/config.json")
def slurper = new JsonSlurper()
def jsonText = f.getText()
config = slurper.parseText(jsonText)

folder('pipelines')

config["pipelines"].each {
    PIPELINE_NAME = it['name']
    RELEASE_VERSION = it['release_version'] ?: '0.0.0'
    LOCKABLE_RESOURCE_LABEL = it['lockable_resource_label']

    pipelineJob('pipelines/' + PIPELINE_NAME) {
        logRotator(-1, MAX_BUILDS, -1, -1)

        parameters {
            stringParam('RELEASE_VERSION', RELEASE_VERSION, 'release version (e.g. 1.0.0)')
            stringParam('LOCKABLE_RESOURCE_LABEL', LOCKABLE_RESOURCE_LABEL, 'lockable resources label')
        }

        definition {
            cps {
                script(readFileFromWorkspace('scripts/qa_pipeline.groovy'))
                sandbox()
            }
        }

    }
}