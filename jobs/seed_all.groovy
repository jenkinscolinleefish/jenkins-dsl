job('seed-all') {

    // Run on master agent
    label('master')

    // Keep last 10 builds
    logRotator(-1, 10, -1, -1)

    // Bitbucket setup
    scm {
        git {
            remote {
                url('git@bitbucket.org:jenkinscolinleefish/jenkins-dsl.git')
                credentials('jenkins-key-to-access-bitbucket')
            }
            extensions {
                cleanAfterCheckout()
            }
            branches('*/master')
        }
    }
    
    triggers {
        bitbucketPush()
    }

    wrappers {
        preBuildCleanup()
    }

    steps {
        dsl {
            external('jobs/*.groovy')
            external('jobs/build/*.groovy')
            external('jobs/test/*.groovy')
            external('jobs/deploy/*.groovy')
            external('jobs/pipelines/*/*.groovy')
            // Default behavior is IGNORE
            // removeAction('IGNORE')
            removeAction('DELETE')
        }
    }
}